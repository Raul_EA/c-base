#include<iostream>
using namespace std;
bool primo(int x) {
    if (x == 0 || x == 1 || x == 4) return false;
    for (int y = 2; y < x / 2; y++) {
        if (x % y == 0) return false;
  }
  return true;
}
int main () {
    int n, x;
    cin>>n;
    for (int i=0; i<n;i++){
        cin>>x;
        if (primo(x)){
           cout<<x<<" es primer"<<endl;
        } else cout<<x<<" no es primer"<<endl;
    }
}
