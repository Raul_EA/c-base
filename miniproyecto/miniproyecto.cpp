#include<iostream>
#include<vector>
#include<fstream>
using namespace std;

struct DATA {
    string nif;
    string per;
    int edad;
    };

void cargar_dat(vector<DATA>& d){
    ifstream ENT("Personas.txt");
    DATA dat;
    while(ENT>>dat.nif>>dat.per>>dat.edad){
        d.push_back(dat);
    }
}

void subir_dat(const vector<DATA>& d){
    ofstream SAL("Personas.txt");
    for(int i=0;i<d.size();i++){
        SAL<<d[i].nif<<" "<<d[i].per<<" "<<d[i].edad<<endl;
    }
}

void buscar(const vector<DATA>& d, string nif, bool& exis,string& per, int& edad){
    for (int i=0;i<d.size();i++){
        if (nif==d[i].nif){
            exis=true;
            per=d[i].per;
            edad=d[i].edad;
            i=d.size();
        } else {
            exis=false;
        }
    }
}

void sumar_per(vector<DATA>& d, string per){
    bool a;
    DATA dat;
    for (int i=0;i<d.size();i++){
        if (per==d[i].per){
            a=true;
            i=d.size();
        } else {
            a=false;
        }
    }
    if (a==true){
        cout<<"Ya existe este nombre"<<endl;
    } else {
        d.push_back(per);
    }

}

int main (){
    string nif,per;
    int edad;
    bool existe;
    vector<DATA> dat(0);
    cargar_dat(dat);
    subir_dat(dat);
    buscar(dat,nif,existe,per,edad);
    sumar_per(dat,per);

}

